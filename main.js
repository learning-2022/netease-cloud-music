import App from './App'

// 引入uView主JS库
import uView from '@/uni_modules/uview-ui'
Vue.use(uView);
// 引入 Vuex
import store from './store'

//请求接口封装 挂载到全局
import api from 'api/request.js'
Vue.prototype.$api = api



// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    store,
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif