import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cookie:`cookie=${uni.getStorageSync('cookie')}`
  },
  mutations: {
   
  },
  actions: {
    
  },
  getters: {
 
  }
})