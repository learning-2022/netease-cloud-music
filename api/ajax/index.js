import md5 from "./md5"

// uni.baseUrl = "http://b080.sxqichuangkeji.com/"
uni.baseUrl = "http://mark.sxqichuangkeji.com/"
// uni.baseUrl = "http://jichu.sxqichuangkeji.com/"
// uni.baseUrl = "/apis/" 


const request = (url = '', data = {}, type = "POST", header = {}) => {
	return new Promise((resolve, reject) => {
		// uni.showLoading({
		// 	title:"加载中...",
		// 	icon:"loading"
		// })
		//从localstorage拿到token

		var token;
		try {
			if (uni.getStorageSync('token') == null || uni.getStorageSync('token') == undefined) {
				token = ""
			} else {
				token = uni.getStorageSync('token')
			}
			//运行代码
		} catch (err) {
			token = ""
			//处理错误

		}
		var header = {
			"api-token": token,
			'Content-Type': 'application/x-www-form-urlencoded'
		}
		//获取时间戳 放到sign里
		const times=new Date().getTime()
		//全局携带参数时间戳
		data.time=times
		//signature 该方法第一个参数请求参数 第二个定义关键字
		let sign = signature(data, 'woshijiamijiekou');
		//全局携带sign
		data.sign = sign;
		uni.request({
			method: type,
			url: uni.baseUrl + url,
			// url: "/apis/" + url,
			data: data,
			header: header,
			dataType: 'json',
		}).then((response) => {
			let [error, res] = response;
			if (res.data.code == 0 && res.data.info) {
				// uni.showToast({
				// 	title: res.data.info,
				// 	icon: "none"
				// })
			}
			//401时 用户登录超时
			if (res.data.code == 401) {
				uni.showToast({
					title: res.data.info,
					icon: "none"
				})
				setTimeout(() => {
					uni.redirectTo({
						url: "/pages/login/login"
					})
				}, 500)
			}
			resolve(res.data);
		}).catch(error => {
			let [err, res] = error;
			reject(err)
		})
	});
}
//md5字段排序加密  data当前传递的参数   key后面自定义加密规则
function signature(data, key) {
	//先判断data里有没有sign 有的话 清除之前的数据
	if (data.sign) {
		delete data.sign;
	}
	//objNull 自定义方法 判断对象里面是否为空
	data = objNull(data)
	var n = null,
		d = {},
		str = '',
		s = ''
		//根据key值排序
	n = Object.keys(data).sort()
	// console.log(n)
	for (var i in n) {
		if (isJSON(data[n[i]])) continue
		d[n[i]] = data[n[i]]

	}
	//拼接参数为字符串
	for (var k in d) {
		if (str != '') str += '&'
		if (isContainChinese(d[k])) {
			str += k + '=' + encodeURIComponent(d[k])
		} else {
			str += k + '=' + encodeURI(d[k])
		}
		// console.log("拼接"+ d[k])
	}
	str += '&key=' + key
	// console.log("拼接之后未加密的字符串"+str.trim())
	s = md5.hex_md5(str).toUpperCase() // 这儿是进行MD5加密并转大写
	// console.log("拼接之后加密的字符串"+s)
	return s
}
// 用来清楚对象里面的数据
function objNull(obj) {
	Object.keys(obj).forEach(item => {
		console.log(obj[item])
		if (!obj[item]){
			obj[item]=""
		}else{
			obj[item]=obj[item].toString().trim()
		}
	})
	return obj;
}

//判断字符串是否是json
function isJSON(str) {
	if (typeof str == 'string') {
		try {
			var obj = JSON.parse(str);
			if (typeof obj == 'object' && obj) {
				return true;
			} else {
				return false;
			}

		} catch (e) {
			return false;
		}
	} else {
		return false;
	}
}
//判断字符串是否包含中文
function isContainChinese(val) {
	if (/.*[\u4e00-\u9fa5]+.*$/.test(val)) {
		return true;
	}
	return false;
}


export default request
